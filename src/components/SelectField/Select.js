import styled from 'styled-components';

const Select = styled.select`
  display: inline-block;
  padding: 0.5rem 1.875rem 0.5rem 0.5rem;
  appearance: none;
  font-size: 2rem;
  border: 0;
  cursor: pointer;
  appearance: none;
  color: #4a4a4a;
  font-weight: 300;
  letter-spacing: 1px;
  background-color: inherit;
  color: #FFF;
  box-shadow: none;
  margin-bottom: 1rem;
`;

export default Select;
