import styled from 'styled-components';

const Widget = styled.div`
  background-color: #ffffff;
  border: 1px solid #818aa3;
  border-radius: 0.25rem;
  font-family: Arial, Helvetica, sans-serif;
`;

export default Widget;
