import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import withInterval from './components/withInterval/withInterval';

import registerServiceWorker from './registerServiceWorker';

const currencies = ['EUR', 'GBP', 'USD'];

const AppWithInterval = withInterval(App, 10000)

ReactDOM.render(
    <AppWithInterval currencies={currencies} />,
    document.getElementById('root')
);
registerServiceWorker();
