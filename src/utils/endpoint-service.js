import { API_KEY, API_URL } from '../config';

const latest = async (symbols) => {
  const response = await fetch(`${API_URL}/latest.json?app_id=${API_KEY}&symbols=${symbols.join(',')}`)
  const json = await response.json();
  const rates = {...json.rates};
  return rates;
}


export {
  latest
}

export default {
  latest
}
