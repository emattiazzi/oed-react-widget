let exchangeRates = {};
let baseCurrency = 'USD';

const set = (rates = {}, baseCurrency = 'USD') => {
  exchangeRates = rates;
  exchangeRates[baseCurrency] = 1;
  return exchangeRates;
};

const convert = (value, from, to) => value * getRate(from, to);

const getRate = (from, to) => {
  if (from === baseCurrency) {
    return exchangeRates[to];
  }

  if (to === baseCurrency) {
    return 1 / exchangeRates[from];
  }

  return exchangeRates[to] * (1 / exchangeRates[from]);
};

export default {
  set,
  convert
};
